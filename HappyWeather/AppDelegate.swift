//
//  AppDelegate.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 24/05/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import UIKit
import UserNotifications
import Alamofire
import SwiftyJSON
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    //private var _almWeatherType: String!
    
    private func requestNotificationAuthorization(application: UIApplication){
        let center = UNUserNotificationCenter.current()
        let options: UNAuthorizationOptions = [.alert, .badge, .sound]
        
        center.requestAuthorization(options: options) { granted, error in
            if let error = error {
                print(error.localizedDescription)
            }
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //Set the minimum intervall to fetch in background the app, the system will run the fetch as often as the system allow
        UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)
        //I pass the method in order to request the authorisation when launch
        requestNotificationAuthorization(application: application)
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    //Questa funzione parte ogni volta che il systema fa partire la background fetch 
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        
        
        let userDefault = UserDefaults.standard
        let notificationPublisher = NotificationPublisher()
        
        var weatherDataList = userDefault.array(forKey: "weatherDataList")
        if weatherDataList!.count > 0{
            var newDate: Date!
            var newCity: String!
            var newWeather: String!
            var newLat: String!
            var newLong: String!
            var position = 0
            for item in weatherDataList!{
                if let date = (item as AnyObject)["Date"] as? String{
                    //newDate = date
                    let formatter = DateFormatter()
                    // initially set the format based on your datepicker date / server String
                    formatter.dateFormat = "yyyy-MM-dd"
                    // convert your string to date
                    newDate = formatter.date(from: date)
                }
                if let city = (item as AnyObject)["City"] as? String{
                    newCity = city
                    
                }
                if let weather = (item as AnyObject)["Weather"] as? String{
                    newWeather = weather
                }
                if let latitude = (item as AnyObject)["Latitude"] as? String{
                    newLat = latitude
                }
                if let longitude = (item as AnyObject)["Longitude"] as? String{
                    newLong = longitude
                }
                
                if let dateToDate = (item as AnyObject)["Date"] as? String{
                    let date = Date()
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd"
                    let newDateOfDate = formatter.string(from: date)
                    print("print newDateOfDate",newDateOfDate)
                    print("dateToDate",dateToDate)
                    if dateToDate <= newDateOfDate{
                        weatherDataList?.remove(at: position)
                        position-=1
                        print(position)
                        print("Ho rimosso i giorni con date trascorse")
                    }
                }
                
                var almWeatherType: String!
                
                if Calendar.current.isDateInToday(newDate){
                    print("ecco la ltitudine",newLat)
                    print("ecco la longitudine", newLong)
                    let API_LOCATION_URL = String(format:
                        "https://api.weatherbit.io/v2.0/current?&lat=%@&lon=%@&key=a2add54842cd4b8d93a121b0f9657096",newLat ,newLong)
                    //passo ad Alamo la mia API che e'in JSON per combinare tutti i dati
                    Alamofire.request(API_LOCATION_URL).responseJSON { (response) in
                        print("sono dentro alamo")
                        //verifico se ho dei risultati
                        let result = response.result
                        print(result)
                        //verifico se il risultato e'positivo e contiene qualcosa
                        if result.isSuccess{
                            let jsonObject = JSON(result.value)
                            // print(jsonObject)
                            // almCity = jsonObject["data"][0]["city_name"].stringValue
                            //almDate = currentDateFromUnix(unixDate: jsonObject["data"][0]["ts"].double)
                            almWeatherType = jsonObject["data"][0]["weather"]["description"].stringValue
                            print("ecco il meteo attuale secondo la API dentro alamo",almWeatherType)
                        }else{
                            print("no result found for the choosen location")
                        }
                        print("ecco il tuo meteo che hai salvato fuori alamo",almWeatherType)
                        print("ecco il meteo che hai salvato in userDefault: ", newWeather)
                        if almWeatherType.lowercased() == newWeather.lowercased() {
                            DispatchQueue.main.async {
                                notificationPublisher.sendNotification(title: "Happy Weather", subtitle: "Hey your forecast is gonna happen!", body: "A \(String(describing: newWeather!)) is expected in \(newCity ?? "nil") today!", badge: 1, delayInterval: 1)//9am 32340
                                print("la notifica è stata inviata")
                            }
                        }
                    }
                }
                position += 1
            }
            userDefault.set(weatherDataList, forKey: "weatherDataList")
            completionHandler(.newData)
        }else{
            completionHandler(.failed)
            print("No such data in to the list")
        }
    }
    
}



