//
//  ForecastCollectionViewCell.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 25/05/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import UIKit

class ForecastCollectionViewCell: UICollectionViewCell {
    
    //outlet
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    
    let hour = Calendar.current.component(.hour, from: Date())
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        switch hour {
        case 1...4:
            timeLabel.textColor = UIColor.white
            tempLabel.textColor = UIColor.white
        case 21...24:
            timeLabel.textColor = UIColor.white
            tempLabel.textColor = UIColor.white
        default:
            timeLabel.textColor = UIColor.black
            tempLabel.textColor = UIColor.black
        }
        
    }
    //funzione che genera una cell dalla classe HourlyForecast, avremo cosi 24 cell per ogni ora che riportiamo
    func generateCell(weather: HourlyForecast){
        timeLabel.text = weather.date.time()
        weatherIcon.image = getWeatherIcon(weather.weatherIcon)
        tempLabel.text = "\(weather.temp)°C"
    }
    
}
