//
//  InfoCollectionViewCell.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 29/05/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import UIKit

class InfoCollectionViewCell: UICollectionViewCell {
    
    //Outlet
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var infoImageView: UIImageView!
    
    let hour = Calendar.current.component(.hour, from: Date())
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // funzione layout per il cambio colore delle label a seconda del backgorund
    override func layoutSubviews() {
        super.layoutSubviews()
        switch hour {
        case 1...4:
            infoLabel.textColor = UIColor.white
            nameLabel.textColor = UIColor.white
        case 21...24:
            infoLabel.textColor = UIColor.white
            nameLabel.textColor = UIColor.white
        default:
            infoLabel.textColor = UIColor.black
            nameLabel.textColor = UIColor.black
        }
    }
    
    
    func generateCell(weatherInfo: WeatherInfo){
        infoLabel.text = weatherInfo.infoText
        //facciamo aggiuntare al sistema l'altezza
        infoLabel.adjustsFontSizeToFitWidth = true
        //essendo che nella cell non ci sono solo immagini ma anche una label per gli UV, controllo se esiste una immagine oppure no e a secondo setto i paramentri rendendo visibile o meno l'immagine o la label 
        if weatherInfo.image != nil{
            nameLabel.isHidden = true
            infoImageView.isHidden = false
            infoImageView.image = weatherInfo.image
        }else{
            nameLabel.isHidden = false
            infoImageView.isHidden = true
            nameLabel.adjustsFontSizeToFitWidth = true
            nameLabel.text = weatherInfo.nameText
            
        }
        
    }
    
}
