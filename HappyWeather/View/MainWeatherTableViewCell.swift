//
//  MainWeatherTableViewCell.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 05/06/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import UIKit

//Classe per la print delle località salvate dall'utente 
class MainWeatherTableViewCell: UITableViewCell {
    
    
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func generateCell(weatherData: CityTempData){
        cityLabel.text = weatherData.city
        cityLabel.adjustsFontSizeToFitWidth = true
        //metto %.0f poiche non voglio decimali altrimenti la API mi passa anche quelli automaticamente, cosi li rimuovo
        tempLabel.text = String(format: "%.0f °C", weatherData.temp)
    }
    
}
