//
//  WeatherTableViewCell.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 29/05/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {
    
    //Outlet
    @IBOutlet weak var dateOfTheWeekLabel: UILabel!
    
    @IBOutlet weak var maxTempOfTheDay: UILabel!
    @IBOutlet weak var minTempOfTheDay: UILabel!
    @IBOutlet weak var weatherIconImage: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func generateCell(forecast: WeeklyWeatherForecast){
        dateOfTheWeekLabel.text = forecast.date.dayOfTheWeek()
        weatherIconImage.image = getWeatherIcon(forecast.weatherIcon)
        maxTempOfTheDay.text = "\(forecast.maxTemp)°C"
        minTempOfTheDay.text = "\(forecast.minTemp)°C"
    }
    
}
