//
//  WeatherView.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 25/05/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import UIKit

class WeatherView: UIView {
    
    //outlet
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var weatherInfoLabel: UILabel!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var bottomContainer: UIView!
    @IBOutlet weak var hourlyCollectionView: UICollectionView!
    @IBOutlet weak var infoCollectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    let hour = Calendar.current.component(.hour, from: Date())
    
    //Var
    //istanza del meteo attuale per passare le info alla UI della classe WeaterView
    var currentWeather: CurrentWeather!
    //Array per la conservazione dei dati (nelle extension) delle cellView e tableView entrambi istanziati come vuoti
    var weeklyWeatherForecastData: [WeeklyWeatherForecast] = []
    var dailyWeatherForecastData: [HourlyForecast] = []
    var weatherInfoData: [WeatherInfo] = []
    
    //inizializer
    override init(frame: CGRect) {
        super.init(frame: frame)
        mainInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        mainInit()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        switch hour {
        case 1...4:
            cityNameLabel.textColor = UIColor.white
            dateLabel.textColor = UIColor.white
            tempLabel.textColor = UIColor.white
            weatherInfoLabel.textColor = UIColor.white
        case 21...24:
            cityNameLabel.textColor = UIColor.white
            dateLabel.textColor = UIColor.white
            tempLabel.textColor = UIColor.white
            weatherInfoLabel.textColor = UIColor.white
        default:
            cityNameLabel.textColor = UIColor.black
            dateLabel.textColor = UIColor.black
            tempLabel.textColor = UIColor.black
            weatherInfoLabel.textColor = UIColor.black
        }
    }
    
    //visto che usiamo .xib necessiatiamo di inizializzare manualmente la view cosa che avrebbe fatto il nostro storybord in automatico se avessi settato tutto li
    private func mainInit(){
        //application bundle, accediamo attraverso a questo elemento al file .xib
        Bundle.main.loadNibNamed("WeatherView", owner: self, options: nil)
        addSubview(mainView)
        
        mainView.frame = self.bounds
        mainView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        setupTableView()
        setupHourlyCollectionView()
        setupInfoCollectionView()
        backgroundMotionEffect(view: weatherIcon, intesity: 15)
    }
    
    //funzioni per la gestione di tableView e colletionView in modo da non dover scrivere tutto nell'init iniziale
    private func setupTableView(){
        tableView.register(UINib(nibName: "WeatherTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "Cell")
        
        tableView.delegate = self
        //prende le informazioni e popla la tabella stessa
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }
    
    private func setupHourlyCollectionView(){
        hourlyCollectionView.register(UINib(nibName: "ForecastCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "Cell")
        
        hourlyCollectionView.dataSource = self
    }
    
    private func setupInfoCollectionView(){
        infoCollectionView.register(UINib(nibName: "InfoCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "Cell")
        infoCollectionView.dataSource = self
    }
    
    //funzione la quale chiamata e'possibile anche da classi esterne a supporto delle private per la gestione dei dati
    func refreshData(){
        setupCurrentWeather()
        setUpWeatherInfo()
        infoCollectionView.reloadData()
    }
    
    //funzione di utilizzo per passare i dati alla view attraverso l'istanza di currentweather
    private func setupCurrentWeather(){
        cityNameLabel.text = currentWeather.city
        dateLabel.text =  currentWeather.date.shortDate()
        weatherInfoLabel.text = currentWeather.weatherType
        tempLabel.text = "\(currentWeather.currentTemp)°C"
        weatherIcon.image = getWeatherIcon(currentWeather.weatherIcon)
    }
    
    //funzione per la crezione degli oggetti per le info aggiuntive
    private func setUpWeatherInfo(){
        //formatto con string per convertire i dati che ricevo dalla API come double nel formato corretto
        let windInfo = WeatherInfo(infoText: String(format: "%.1f m/sec", currentWeather.windSpeed), nameText: nil, image: getWeatherIcon("wind"))
        
        let humidityInfo = WeatherInfo(infoText: String(format: "%.0f", currentWeather.humidity), nameText: nil, image: getWeatherIcon("humidity"))
        
        let pressureInfo = WeatherInfo(infoText: String(format: "%.0f mb", currentWeather.pressure), nameText: nil, image: getWeatherIcon("pressure"))
        
        let visibilityInfo = WeatherInfo(infoText: String(format: "%.0f km", currentWeather.visibility), nameText: nil, image: getWeatherIcon("visibility"))
        
        let feelsLikeInfo = WeatherInfo(infoText: String(format: "%.1f", currentWeather.feelsLike), nameText: nil, image: getWeatherIcon("feelsLike"))
        
        let uvInfo = WeatherInfo(infoText: String(format: "%.1f", currentWeather.uv), nameText: "UV index", image:nil)
        
        let sunriseInfo = WeatherInfo(infoText: currentWeather.sunrise, nameText: nil, image: getWeatherIcon("sunrise"))
        
        let sunsetInfo = WeatherInfo(infoText: currentWeather.sunset, nameText: nil, image: getWeatherIcon("sunset"))
        
        //inserisco tutto nel mio array per passare poi i dati da visualizzare
        weatherInfoData = [windInfo, humidityInfo, pressureInfo, visibilityInfo, feelsLikeInfo, uvInfo, sunriseInfo, sunsetInfo]
    }
    
    //Funzione per il movimento della icona al roteare del dispositivo 
    func backgroundMotionEffect(view: UIView, intesity: Double){
        //asse delle X
        let horizontalMotion = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        horizontalMotion.minimumRelativeValue = -intesity
        horizontalMotion.maximumRelativeValue = intesity
        
        //asse delle y
        let verticalMotion = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        verticalMotion.minimumRelativeValue = -intesity
        verticalMotion.maximumRelativeValue = intesity
        
        //add the movment of the backgorud
        let moveBackground = UIMotionEffectGroup()
        moveBackground.motionEffects = [horizontalMotion, verticalMotion]
        
        view.addMotionEffect(moveBackground)
    }
    
}

//estensioni per la gestione delle cellView e la tableView le quali ci consento il popolamento di entrambe
extension WeatherView: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return weeklyWeatherForecastData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! WeatherTableViewCell
        cell.generateCell(forecast: weeklyWeatherForecastData[indexPath.row])
        return cell
    }
}

// le collectionView hanno un minimo di due funzioni richieste per funzionare
extension WeatherView: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == hourlyCollectionView{
            return dailyWeatherForecastData.count
        }else{
            return weatherInfoData.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == hourlyCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ForecastCollectionViewCell
            cell.generateCell(weather: dailyWeatherForecastData[indexPath.row])
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! InfoCollectionViewCell
            cell.generateCell(weatherInfo: weatherInfoData[indexPath.row])
            return cell
        }
    }
}
