//
//  CurrentWeather.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 24/05/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class CurrentWeather {
    
    private var _city: String!
    private var _date: Date!
    private var _currentTemp: Double!
    private var _feelsLike: Double!
    private var _uv: Double!
    
    private var _weatherType: String!
    private var _pressure: Double! //mb format
    private var _humidity: Double! //%
    private var _windSpeed: Double! //meter/sec
    private var _weatherIcon: String!
    private var _visibility: Double!
    private var _sunrise: String!
    private var _sunset: String!
    
    //variabili pubbliche per accedere dall'esterno
    var  city: String {
        if _city == nil{
            _city = ""
        }
        return _city
    }
    
    var  date: Date {
        if _date == nil{
            _date = Date()
        }
        return _date
    }
    
    var  currentTemp: Double {
        if _currentTemp == nil{
            _currentTemp = 0.0
        }
        return _currentTemp
    }
    
    var  feelsLike: Double {
        if _feelsLike == nil{
            _feelsLike = 0.0
        }
        return _feelsLike
    }
    
    var  uv: Double {
        if _uv == nil{
            _uv = 0.0
        }
        return _uv
    }
    
    var  weatherType: String {
        if _weatherType == nil{
            _weatherType = ""
        }
        return _weatherType
    }
    
    var  pressure: Double {
        if _pressure == nil{
            _pressure = 0.0
        }
        return _pressure
    }
    
    var  humidity: Double {
        if _humidity == nil{
            _humidity = 0.0
        }
        return _humidity
    }
    
    var  windSpeed: Double {
        if _windSpeed == nil{
            _windSpeed = 0.0
        }
        return _windSpeed
    }
    
    var  weatherIcon: String {
        if _weatherIcon == nil{
            _weatherIcon = ""
        }
        return _weatherIcon
    }
    
    var  visibility: Double {
        if _visibility == nil{
            _visibility = 0.0
        }
        return _visibility
    }
    
    var  sunrise: String {
        if _sunrise == nil{
            _sunrise = ""
        }
        return _sunrise
    }
    
    var  sunset: String {
        if _sunset == nil{
            _sunset = ""
        }
        
        return _sunset
    }
    
    //@escaping lo utilizziamo tutte le volte che la funzione ha completato il download dei dati cosi sono ad allora trasferiamo il tutto alle classi che si occuperanno di mandare in display i dati
    func downloadCurrentWeather(location: WeatherLocation, completion: @escaping(_ success: Bool)-> Void){
        
        var API_LOCATION_URL: String!
        //Se sto cercando una location che non è la attuale: con @% cambio l'argomento con quello nell'argument (local), sostanzialmente è un placeholder
        if !location.isCurrentLocation{
            API_LOCATION_URL = String(format:
                "https://api.weatherbit.io/v2.0/current?&lat=%@&lon=%@&key=a2add54842cd4b8d93a121b0f9657096", location.lat, location.long)
        }else{
            //Al contraio uso il gps e prendo la current
            API_LOCATION_URL = CURRENT_LOCATION_URL
        }
        
        //passo ad Alamo la mia API per combinare tutti i dati
        Alamofire.request(API_LOCATION_URL).responseJSON { (response) in
            
            //assegno il risultato della response ad una variabile
            let result = response.result
            
            //verifico se il risultato e'positivo e contiene qualcosa
            if result.isSuccess{
                //creo una variabile che mi converta con SwiftyJSON il risultato in un JSON
                let jsonObject = JSON(result.value)
                // print(jsonObject)
                self._city = jsonObject["data"][0]["city_name"].stringValue
                self._date = currentDateFromUnix(unixDate: jsonObject["data"][0]["ts"].double)
                self._weatherType = jsonObject["data"][0]["weather"]["description"].stringValue
                self._weatherIcon = jsonObject["data"][0]["weather"]["icon"].stringValue
                self._currentTemp = jsonObject["data"][0]["temp"].double
                
                self._feelsLike = jsonObject["data"][0]["app_temp"].double
                self._pressure = jsonObject["data"][0]["pres"].double
                self._humidity = jsonObject["data"][0]["rh"].double
                self._windSpeed = jsonObject["data"][0]["wind_spd"].double
                self._visibility = jsonObject["data"][0]["vis"].double
                self._uv = jsonObject["data"][0]["uv"].double
                self._sunrise = jsonObject["data"][0]["sunrise"].stringValue
                self._sunset = jsonObject["data"][0]["sunset"].stringValue
                
                //se le informazioni che abbiamo recipito hanno successo chiamo il true
                completion(true)
            }else{
                //se la API non mi rileva dati sulla città per qualsiasi motivo e l'utente la aggiunge hai preferiti, setto il valore in modo tale che userDefault riesca ad elimnirare dalla lista anche questo caso in quanto altrimenti non riuscirebbe poichè non ha paramentri visto che i campi sono tutti vuoti
                self._city = location.city
                //se le info ritornate non ci sono o non vanno bene chiamo il false
                completion(false)
                print("no result found for the current location")
            }
        }
        
    }
}

