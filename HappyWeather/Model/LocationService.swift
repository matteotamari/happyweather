//
//  LocationService.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 05/06/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import Foundation

class LocationService {
    
    //Creo un sigleton per la gestione delle coordinate
    static var shared = LocationService()
    
    var longitude: Double!
    var latitude: Double!
}
