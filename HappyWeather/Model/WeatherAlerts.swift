//
//  WeatherAlerts.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 21/06/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class WeatherAlerts{
    private var _city: String!
    private var _severity: String!
    private var _title: String!
    private var _description: String!
    
    var city: String{
        if _city == nil{
            _city = ""
        }
        return _city
    }
    
    var severity: String{
        if _severity == nil {
            _severity = ""
        }
        return _severity
    }
    
    var title: String{
        if _title == nil{
            _title = ""
        }
        return _title
    }
    
    var  description: String{
        if _description == nil{
            _description = ""
        }
        return _description
    }
    

     func downloadWeatherAlerts(completion: @escaping(_ success: Bool)-> Void){

        let API_ALERTS_URL =  CURRENT_LOCATION_WEATHER_ALERTS
    
        Alamofire.request(API_ALERTS_URL).responseJSON { (response) in
        
        //verifico se ho dei risultati
        let result = response.result
        
        //verifico se il risultato e'positivo e contiene qualcosa
        if result.isSuccess{
            let jsonObject = JSON(response.value)
            
            self._city = jsonObject["city_name"].stringValue
            self._title = jsonObject["alerts"][0]["title"].stringValue
            self._description = jsonObject["alerts"][0]["description"].stringValue
            self._severity = jsonObject["alerts"][0]["severity"].stringValue
            print("ecco la descrizione",self._description)
            completion(true)
            
            print("ecco la città",self._city)
        }else{
            completion(false)
            print("no result found for the current location")
        }
    }
}
}
