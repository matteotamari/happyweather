//
//  ChartsForecast.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 09/06/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ChartsForecast {
    private var _precip: Double!
    private var _temp: Double!
    private var _city: String!
    private var _lat: String!
    private var _long: String!
    
    var precip: Double{
        if _precip == nil{
            _precip = 0.0
        }
        return _precip
    }
    
    var temp: Double{
        if _temp == nil{
            _temp = 0.0
        }
        return _temp
    }
    
    
    init(weatherDictionary: Dictionary<String, AnyObject>){
        
        let jsonObject = JSON(weatherDictionary)
        
        self._temp = jsonObject["temp"].double
        self._precip = jsonObject["precip"].double
        //print("your precipitation: ",self._precip)
        //print("your temperature: ",self._temp)
    }
    
    class func downloadChartForecast(completion: @escaping (_ weatherForecast: [ChartsForecast]) -> Void){
        
        let API_WEEKLY_FORECAST_URL = CURRENT_LOCATION_CHARTS_WEEKLYFORECAST_URL
        
        Alamofire.request(API_WEEKLY_FORECAST_URL).responseJSON {(response) in
            
            let result = response.result
            
            //assegno a vuoto l'array per gestire il caso nell'if else sotto
            var forecastArray: [ChartsForecast] = []
            print(result.isSuccess)
            if result.isSuccess{
                
                if let dictionary = result.value as?  Dictionary<String, AnyObject>{
                    if let list = dictionary["data"] as?  [Dictionary<String, AnyObject>]{
                        
                        for item in list{
                            let forecast = ChartsForecast(weatherDictionary: item)
                            forecastArray.append(forecast)
                            print(forecastArray.count)
                        }
                    }
                }
                completion(forecastArray)
            }else {
                completion(forecastArray)
            }
        }
    }
}
