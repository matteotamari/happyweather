//
//  WeeklyWeatherForecast.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 25/05/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class WeeklyWeatherForecast{
    
    private var _date: Date!
    private var _temp: Double!
    private var _weatherIcon: String!
    private var _minTemp: Double!
    private var _maxTemp: Double!
    
    //variabili pubbliche per accedere dall'esterno
    var date: Date{
        if _date == nil{
            _date = Date()
        }
        return _date
    }
    
    var temp: Double{
        if _temp == nil{
            _temp = 0.0
        }
        return _temp
    }
    
    var  weatherIcon: String {
        if _weatherIcon == nil{
            _weatherIcon = ""
        }
        return _weatherIcon
    }
    
    var minTemp: Double{
        if _minTemp == nil{
            _minTemp = 0.0
        }
        return _minTemp
    }
    
    var maxTemp: Double{
        if _maxTemp == nil{
            _maxTemp = 0.0
        }
        return _maxTemp
    }
    
    
    //Per ogni risultato che ci ritorna la nostra API estraiamo il dato e lo passiamo alle variabili attraverso il paramentro in ingresso weatherDictionary che prende in input un dizionario
    init(weatherDictionary: Dictionary<String, AnyObject>){
        
        let jsonObject = JSON(weatherDictionary)
        
        self._temp = jsonObject["temp"].double
        self._minTemp = jsonObject["min_temp"].double
        self._maxTemp = jsonObject["max_temp"].double
        self._date = currentDateFromUnix(unixDate: jsonObject["ts"].double!)
        self._weatherIcon = jsonObject["weather"]["icon"].stringValue
        
    }
    
    //@escaping lo utilizziamo tutte le volte che la funzione ha completato il download dei dati cosi sono ad allora trasferiamo il tutto alle classi che si occuperanno di mandare in display i dati
    class func downloadWeeklyWeatherForecast(location:WeatherLocation, completion: @escaping (_ weatherForecast: [WeeklyWeatherForecast]) -> Void){
        
        
        var API_WEEKLY_FORECAST_URL: String!
        //Se sto cercando una location che non è la attuale: con @% cambio l'argomento con quello nell'argument (local), sostanzialmente è un placeholder
        if !location.isCurrentLocation{
            API_WEEKLY_FORECAST_URL = String(format: "https://api.weatherbit.io/v2.0/forecast/daily?&lat=%@&lon=%@&days=7&key=a2add54842cd4b8d93a121b0f9657096", location.lat, location.long)
        }else{
            //Al contraio uso il gps e prendo la current
            API_WEEKLY_FORECAST_URL = CURRENT_LOCATION_WEEKLYFORECAST_URL
        }
        
        //passo ad Alamo la mia API per combinare tutti i dati
        Alamofire.request(API_WEEKLY_FORECAST_URL).responseJSON {(response) in
            
            //assegno il risultato della response ad una variabile
            let result = response.result
            
            //assegno a vuoto l'array per gestire il caso nell'if else sotto
            var forecastArray: [WeeklyWeatherForecast] = []
            
            if result.isSuccess{
                
                if let dictionary = result.value as?  Dictionary<String, AnyObject>{
                    if var list = dictionary["data"] as?  [Dictionary<String, AnyObject>]{
                        
                        //rimuovo il primo in quanto e' il meteo della giornata attuale
                        list.remove(at: 0)
                        //print("number of day", list.count)
                        
                        for item in list{
                            let forecast = WeeklyWeatherForecast(weatherDictionary: item)
                            forecastArray.append(forecast)
                        }
                    }
                }
                 //ritorno l'array che ho creato con tutte houry forcast che ho estratto dalla ¢API
                completion(forecastArray)
            }else {
                completion(forecastArray)
            }
        }
        
    }
}
