//
//  WeatherLocation.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 31/05/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import Foundation

//Equatable: la mia location può essere comparata con un altra location 
struct WeatherLocation: Codable, Equatable {
    var cityId:String!
    var city: String!
    var regionId: String!
    var country: String!
    var countryCode: String!
    var lat: String!
    var long: String!
    var isCurrentLocation: Bool!
}
