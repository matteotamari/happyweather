//
//  HourlyForecast.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 25/05/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class HourlyForecast {
    
    private var _date: Date!
    private var _temp: Double!
    private var _weatherIcon: String!
    
     //variabili pubbliche per accedere dall'esterno
    var date: Date{
        if _date == nil{
            _date = Date()
        }
        return _date
    }
    
    var temp: Double{
        if _temp == nil{
            _temp = 0.0
        }
        return _temp
    }
    
    var  weatherIcon: String {
        if _weatherIcon == nil{
            _weatherIcon = ""
        }
        return _weatherIcon
    }
    
    init(weatherDictionary: Dictionary<String, AnyObject>){
        
        let jsonObject = JSON(weatherDictionary)
        self._temp = jsonObject["temp"].double
        self._date = currentDateFromUnix(unixDate: jsonObject["ts"].double!)
        self._weatherIcon = jsonObject["weather"]["icon"].stringValue
    }
    
    //@escaping lo utilizziamo tutte le volte che la funzione ha completato il download dei dati cosi sono ad allora trasferiamo il tutto alle classi che si occuperanno di mandare in display i dati
    class func downloadHourlyForecastWeather(location:WeatherLocation, completion: @escaping (_ hourlyForecast: [HourlyForecast]) -> Void){
        
        var API_HOURLY_FORECAST_URL: String!
        //Se sto cercando una location che non è la attuale: con @% cambio l'argomento con quello nell'argument (local), sostanzialmente è un placeholder
        if !location.isCurrentLocation{
            API_HOURLY_FORECAST_URL = String(format: "https://api.weatherbit.io/v2.0/forecast/hourly?&lat=%@&lon=%@&hours=24&key=a2add54842cd4b8d93a121b0f9657096", location.lat, location.long)
        }else{
            //Al contraio uso il gps e prendo la current
            API_HOURLY_FORECAST_URL = CURRENT_LOCATION_HOURLYFORECAST_URL
        }
        
        //passo ad Alamo la mia API per combinare tutti i dati
        Alamofire.request(API_HOURLY_FORECAST_URL).responseJSON { (response) in
            //assegno il risultato della response ad una variabile
            let result = response.result
            
            var forecastArray: [HourlyForecast] = []
            if result.isSuccess{
                if let dictionary = result.value as? Dictionary<String, AnyObject>{
                    //controllo solo la lista nel dizionario che mi interessa ovvero la hourly forecast ovvero la "data"
                    if let list = dictionary["data"] as? [Dictionary<String, AnyObject>] {
                        for item in list{
                            let forecast = HourlyForecast(weatherDictionary: item)
                            forecastArray.append(forecast)
                        }
                    }
                }
                //ritorno l'array che ho creato con tutte houry forcast che ho estratto dalla ¢API
                completion(forecastArray)
            }else {
                print("there is no hourly data")
                completion(forecastArray )
            }
        }
    }
    
}
