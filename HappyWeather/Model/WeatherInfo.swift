//
//  WeatherInfo.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 29/05/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import Foundation
import UIKit
struct WeatherInfo {
    var infoText: String!
    //le setto a optional perchè nell cell solo un il dato della UV non possiede una immagine ma bensì una Label
    var nameText: String?
    //optional perchè non tutte le le weather info hanno delle image
    var image: UIImage?
}
