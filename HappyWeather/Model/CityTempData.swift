//
//  CityTempData.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 05/06/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import Foundation

struct CityTempData {
    var city: String!
    var temp: Double!
}
