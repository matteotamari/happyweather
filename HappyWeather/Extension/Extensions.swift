//
//  Extensions.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 25/05/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import Foundation

extension Date {
    //essendo la data di tipo date utilizza questa estensione di Date per passagli una stringa
    func shortDate() -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd,yyyy"
        return dateFormatter.string(from: self)
    }
    //funzione per ritornare solo l'orario che sara'visto nella cellView delle previsioni orarie della giornata
    func time() -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: self)
    }
    
    func dayOfTheWeek() -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self)
    }
}


