//
//  WeatherViewController.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 25/05/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import UIKit
import CoreLocation

class WeatherViewController: UIViewController {
    
    
    //Outlet
    @IBOutlet weak var weatherScrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var backgroungImage: UIImageView!
    
    let userDefault = UserDefaults.standard
    let hour = Calendar.current.component(.hour, from: Date())
    
    var locationManager: CLLocationManager?
    var currentLocation: CLLocationCoordinate2D!
    
    var allLocations: [WeatherLocation] = []
    var allWeatherViews: [WeatherView] = []
    var allWeatherData: [CityTempData] = []
    var changeLabelColor = WeatherView()
    //variabile per far fare il refresh della pagina o meno, se l'utente elimina una località allora voglio che la weatherView page esegua un refresh in questo modo non vedrà la località eliminata, altrimenti se clicca solo sulla località non voglio eseguire il refresh della pagina
    var shouldRefresh = true
    
    //View lifeycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //print(weatherScrollView.bounds)
        locationManagerStart()
        weatherScrollView.delegate = self
        print(hour)
        
        switch hour {
        case 1...4:
            backgroungImage.image = UIImage(named:"bgNight@x1.jpg")
        case 5...20:
            backgroungImage.image =  UIImage(named: "bgDay@x1.jpg")
        case 21...24:
            backgroungImage.image = UIImage(named:"bgNight@x1.jpg")
        default:
            backgroungImage.image = UIImage(named: "bgDay@x1.jpg")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if shouldRefresh{
            allLocations = []
            allWeatherViews = []
            removeViewsFromScrollView()
            locationAuthCheck()
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //se l'utente interrome l'utilizzo chiudendo la view della app fermo il locationManager per preservare la batteria
        locationManagerStop()
    }
    
    
    //Download Weather
    //collegamento tra il model la view attraverso il controller come MVC richiede
    private func getCurrentWeather(weatherView: WeatherView, location: WeatherLocation){
        weatherView.currentWeather = CurrentWeather()
        //faccio il richiamo al metodo della classe CurrentWeather per ricevere i dati scaricati utilizzando la location che gli passo
        weatherView.currentWeather.downloadCurrentWeather(location: location) { (success) in
            weatherView.refreshData()
            //chiamo funzione per il display delle località salvate in quanto mi interessa vedere la citta e temperatura attuale della località in questione
            self.generateWeatherList()
        }
    }
    
    
    private func getWeeklyWeather(weatherView: WeatherView, location: WeatherLocation){
        WeeklyWeatherForecast.downloadWeeklyWeatherForecast(location: location) { (weatherForecast) in
            weatherView.weeklyWeatherForecastData = weatherForecast
            weatherView.tableView.reloadData()
        }
    }
    
    
    private func getHourlyWeather(weatherView: WeatherView, location: WeatherLocation){
        HourlyForecast.downloadHourlyForecastWeather(location: location) { (weatherForecast) in
            weatherView.dailyWeatherForecastData = weatherForecast
            weatherView.hourlyCollectionView.reloadData()
        }
    }
    
    // Download Weather
    private func getWeather(){
        loadLocationFromUserDefault()
        createWeatherView()
        addWeatherToScrollView()
        setPageControllPageNumber()
    }
    
    //con questa funzione rimuovo i vecchi dati e sostanzialmente li riscrivo, in quanto se l'utente compie operazioni di cancellazioni o aggiunge ricarico la view inetera, se questo non avviene mi crea un bug tsle per cui si sdoppiano le stampe a display (stackoverflow)
    private func removeViewsFromScrollView() {
        for view in weatherScrollView.subviews {
            view.removeFromSuperview()
        }
    }
    
    //funzione che mi valuta quante location sono presenti nell'array e va a creare una view per ognuna sulla WeatherVierw class
    private func createWeatherView(){
        for location in allLocations{
            allWeatherViews.append(WeatherView())
        }
    }
    
    private func addWeatherToScrollView(){
        // print("All weather view has \(allWeatherViews.count)")
        //scorro tutti gli oggetti nella weather view e per ognuno di questi li inserisco nella mia scroll view
        for i in 0..<allWeatherViews.count{
            //print("i is \(i)")
            let weatherView = allWeatherViews[i]
            //faccio la stessa cosa per le location scorro il mio array
            let location = allLocations[i]
            //inserisco i dati nelle varie view sfruttando le funzioni create
            getCurrentWeather(weatherView: weatherView, location: location)
            getHourlyWeather(weatherView: weatherView, location: location)
            getWeeklyWeather(weatherView: weatherView, location: location)
            //essendo una scroll view dinamica che scoirre a dx o sx creo una variabile per tenere traccia della posizione della mia X
            let xPos = self.view.frame.width * CGFloat(i)
            weatherView.frame = CGRect(x: xPos, y: 0, width: weatherScrollView.bounds.width, height: weatherScrollView.bounds.height)
            //inserisco la view con i paramentri passati e X settata
            weatherScrollView.addSubview(weatherView)
            //(i+1) mi serve per avere quel leggero margine durante lo scroll laterale per cambiare la location in modo tale che si veda sia la precendete che la nuova e dia límpressione di essere uno slide continuo
            weatherScrollView.contentSize.width = weatherView.frame.width * CGFloat(i+1)
        }
    }
    
    //Load location from user default
    private func loadLocationFromUserDefault(){
        //inserisco alla localita' attuale dell'utente nell'array
        let currentLocation = WeatherLocation(cityId: "", city: "", regionId: "", country: "", countryCode: "", lat: "", long: "", isCurrentLocation: true)
        
        if let data = userDefault.value(forKey: "Locations") as? Data{
            allLocations = try! PropertyListDecoder().decode(Array<WeatherLocation>.self, from: data)
            //inserisco la mia attuale localita' all'indice 0 del mio array
            allLocations.insert(currentLocation, at: 0)
            
        }else{
            print("No user data in user defaults")
            allLocations.append(currentLocation)
        }
    }
    
    
    //Page controll
    
    //funzioni per la gestione della pagina e mostrare i .. che corrispondono alle località effettivamente inserite dall'utente
    private func setPageControllPageNumber(){
        pageControl.numberOfPages = allWeatherViews.count
    }
    
    private func updatePageControllSelectedPage(currentPage: Int){
        pageControl.currentPage = currentPage
    }
    //Location Manager
    private func locationManagerStart(){
        
        if locationManager == nil{
            locationManager = CLLocationManager()
            locationManager!.desiredAccuracy = kCLLocationAccuracyBest
            locationManager!.requestWhenInUseAuthorization()
            locationManager!.delegate = self
        }
        
        locationManager!.startMonitoringSignificantLocationChanges()
    }
    
    private func locationManagerStop(){
        if locationManager != nil{
            locationManager?.stopMonitoringSignificantLocationChanges()
        }
    }
    
    //funzione per verificare se l'utente ha fornito l'autorizzazione a tracciare la posizione al dispositivo
    private func locationAuthCheck(){
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
            currentLocation = locationManager!.location?.coordinate
            
            if currentLocation != nil{
                //set our coordinate
                LocationService.shared.latitude = currentLocation.latitude
                LocationService.shared.longitude = currentLocation.longitude
                
                getWeather()
            }else{
                locationAuthCheck()
            }
        }else{
            //se non ha autorizzato ripeto l'operazione e richiedo l'autorizzazione nuovamente
            locationManager?.requestWhenInUseAuthorization()
            locationAuthCheck()
        }
    }
    
    private func generateWeatherList(){
        //istanzio l'array a 0 in caso abbia degli oggetti al suo interno in modo tale da non avere lo stesso oggetto da cliccare più e più volte altrimenti l'array può diventare troppo grande perche continua ad inserire gli elementi per il numero degli stessi del tipo ho 7 elementi li inserisce tutti e 7 per 7 volte quindi 49
        allWeatherData = []
        
        for weatherView in allWeatherViews {
            allWeatherData.append(CityTempData(city: weatherView.currentWeather.city, temp: weatherView.currentWeather.currentTemp))
        }
        // print("We have \(allWeatherData.count) number of locations")
    }
    
    //Navigation
    //funzione per settare il seuge della pagina successiva e visualizzare cosi le location salavte nella view prevista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "allLocationSegue"{
            let viewController = segue.destination as! AllLocationTableViewController
            viewController.weatherData = allWeatherData
            viewController.delegate = self
        }
    }
    
    
}

extension WeatherViewController: AllLocationTableViewControllerDelegate{
    func didChooseLocation(atIndex: Int, shouldRefresh: Bool) {
        let viewNumber = CGFloat(integerLiteral: atIndex)
        //in modo che al click di una determinata location l'ultente venga mandato alla view di quella location
        let newOffset = CGPoint(x: (weatherScrollView.frame.width + 1.0) * viewNumber, y: 0)
        
        weatherScrollView.setContentOffset(newOffset, animated: true)
        //effuto anch el'update del page control cosi da essere in linea con la location presentata
        updatePageControllSelectedPage(currentPage: atIndex)
        self.shouldRefresh = shouldRefresh
    }
}

//location manager per gestire la posizione dell'utente e i premessi necessari
extension WeatherViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        // print("Faild to get location, \(error.localizedDescription)")
    }
}

//estensione per consentire al page controller di mostrare lo scorrimento della pagina e il tracciamento della posizione dell'utente nella pagina stessa attraverso i ..
extension WeatherViewController: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let value = scrollView.contentOffset.x / scrollView.frame.size.width
        updatePageControllSelectedPage(currentPage: Int(round(value)))
    }
}


