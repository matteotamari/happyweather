//
//  ChartsDaysViewController.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 13/06/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import UIKit


class ChartsDaysViewController: UIViewController {
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var superButton: CustomButton!
    
    var pickerViewData: [Int] = [Int]()
    var numberOfDays: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        setUpPickerData()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func continueButton(_ sender: Any) {
        superButton.shakeBottomButton()
        performSegue(withIdentifier: "chooseNumberOfDays", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let viewController = segue.destination as! ChartsViewController
        viewController.chooseDay = numberOfDays
    }
    
    private func setUpPickerData(){
        pickerViewData = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
        
    }
}

extension ChartsDaysViewController: UIPickerViewDelegate{
}

extension ChartsDaysViewController: UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerViewData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(pickerViewData[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //quanto l'utente preme su un determinato tipo, qua si scrivere quello che deve fare
        numberOfDays = pickerViewData[row]
        //print("ecco i tuoi giorni ",numberOfDays)
    }
    
}
