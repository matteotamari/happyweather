//
//  AllLocationTableViewController.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 31/05/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import UIKit

//Creo un protocollo per passare la info alla weatherView e dirgli quale località deve mostrare a seconda della scelta dell'utente
protocol AllLocationTableViewControllerDelegate {
    func didChooseLocation(atIndex: Int, shouldRefresh: Bool)
}

class AllLocationTableViewController: UITableViewController {
    
    let userDefault = UserDefaults.standard
    
    var savedLocation: [WeatherLocation]?
    var weatherData: [CityTempData]?
    //delegate del protocollo mi serve per notificare che l'utente ha scelto una specifica location
    var delegate: AllLocationTableViewControllerDelegate?
    var shouldRefresh = false
    
    
    
    //View lidecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        loadFromUserDefault()
    }
    
    
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        //se il mio array e' vuoto allora ritorno 0 questo perche la funzione richiede un tipo di ritorno Int
        //return weatherData?.count ?? 0
        if weatherData != nil{
            return weatherData!.count
        }else{
            return 0
        }
    }
    
    //UserDefault
    private func loadFromUserDefault(){
        if let data = userDefault.value(forKey: "Locations") as? Data{
            savedLocation =  try? PropertyListDecoder().decode(Array<WeatherLocation>.self, from: data)
            //verifico che ci sia qualcosa in user default
            //print("We have \(savedLocation?.count) in user default")
        }
    }
    
    //funzione per la rimozione dellla località salvata selezionata dall'utente (gli passo una stringa che corrisponde al nome della location salvata in UD)
    private func removeLocationFromSavedLocation(location: String){
        if savedLocation != nil{
            for i in 0..<savedLocation!.count{
                let tempLocation = savedLocation![i]
                
                if tempLocation.city == location{
                    savedLocation!.remove(at: i)
                    //salvo nuovamente i dati in UD
                    saveNewLocationFromUserDefault()
                    return 
                }
            }
        }
    }
    
    private func saveNewLocationFromUserDefault(){
        shouldRefresh = true
        userDefault.set(try? PropertyListEncoder().encode(savedLocation!), forKey: "Locations")
        userDefault.synchronize()
    }
    
    //TableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //cosi quanto seleziona non rimane evidenziato e viene deselezionato automaticamente
        tableView.deselectRow(at: indexPath, animated: true)
        //ritorno l'informazione all weatherView per comunicare quale località deve mostrare al click dell'utente
        delegate?.didChooseLocation(atIndex: indexPath.row, shouldRefresh: shouldRefresh)
        self.dismiss(animated: true, completion: nil)
    }
    
    //restituisce true o false a seconda se l'utente puo' o non puo' fare modifche alla view del tipo eliminare le lacation
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        //la prima riga e la current location e non voglio che l'utente possa eliminarla
        if indexPath.row == 0{
            return false
        }else{
            return true
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            let locationToDelete = weatherData?[indexPath.row]
            weatherData?.remove(at: indexPath.row)
            
            //cancello da user defalut
            removeLocationFromSavedLocation(location: locationToDelete!.city)
            tableView.reloadData()
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MainWeatherTableViewCell
        
        if weatherData != nil{
            cell.generateCell(weatherData: weatherData![indexPath.row])
        }
        
        return cell
    }
    
    //funzione per dire a ChooseCity che l'untente ha premuto il bottone + per aggiungere una località
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "chooseLocationSegue"{
            let viewController = segue.destination as! ChooseCityViewController
            viewController.delegate = self
        }
    }
}

extension AllLocationTableViewController: ChooseCityViewControllerDelegate{
    func didAdd(newLocation: WeatherLocation) {
        //print("Added new location", newLocation.country, newLocation.city)
        shouldRefresh = true
        weatherData?.append(CityTempData(city: newLocation.city, temp: Double.nan))
        tableView.reloadData()
    }
}
