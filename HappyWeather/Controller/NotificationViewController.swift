//
//  NotificationViewController.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 11/06/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import Alamofire
import SwiftyJSON
import Toast_Swift

class NotificationViewController: UIViewController {
    
    @IBOutlet weak var chooseCityLabel: UILabel!
    @IBOutlet weak var weatherPicker: UIPickerView!
    @IBOutlet weak var datePicket: UIDatePicker!
    @IBOutlet weak var confirmButton: UIButton!
    
    @IBOutlet weak var superchooseCityButton: CustomButton!
    @IBOutlet weak var superButton: CustomButton!
    var city: String!
    var date: Date!
    var weatherType: String! = "Clear sky"
    
    var selectedDate: String!
    var latitude: String!
    var longitude: String!
    var weatherType2: String!
    var cityType: String!
    var pickerViewData: [String] = [String]()
    var dateFormatter = DateFormatter()
    var bottomCustomButton = CustomButton()
    
    let userDefault = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.weatherPicker.delegate = self
        self.weatherPicker.dataSource = self
        // Do any additional setup after loading the view.
        setPickerData()
        confirmButton.isHidden = true
    }
    
    //funzione per salvare in UD i paramentri inseriti dall'utente per settare poi la notifica
    @IBAction func saveToUserDefault(_ sender: Any){
        superButton.shakeBottomButton()
        superchooseCityButton.shakeBottomButton()
        cityType = chooseCityLabel.text ?? "nil"
        //print(cityType)
        //print(weatherType)
        //print("la longitudine", longitude)
        //print("la latitudine",latitude)
        dateFormatter.dateFormat = "YYYY-MM-dd"
        selectedDate = dateFormatter.string(from: datePicket.date)
        //print(selectedDate)
        let request = ["City": cityType, "Weather": weatherType, "Date": selectedDate, "Latitude": latitude, "Longitude": longitude]
        var pendingNotification = userDefault.array(forKey: "weatherDataList")
        if pendingNotification != nil{
            pendingNotification?.append(request)
        }else{
            pendingNotification = []
            pendingNotification?.append(request)
        }
        userDefault.set(pendingNotification, forKey: "weatherDataList")
        //self.view.makeToast("Your notification has been inserted!")
        self.view.makeToast("Your notification has been set! We will advise you if the forcast will be expected ", duration: 4.0, position: .top, image: UIImage(named: "HappyWeatherIcon.jpg"))
        print("ecco il tuo userDefault", pendingNotification ?? nil!)
    }
    
    //Funzione per il setting del data picker sulla quale l'utente scegli la codizione meteo
    private func setPickerData(){
        pickerViewData = ["Clear sky", "Snow", "Light snow", "Heavy Snow","Mix snow/rain","Sleet","Heavy sleet","Snow shower","Heavy snow shower","Flurries","Mist","Smoke","Haze","Sand/dust","Fog","Freezing Fog", "Thunderstorm with drizzle", "Thunderstorm with light rain", "Thunderstorm with rain","Thunderstorm with rain","Thunderstorm with heavy rain","Thunderstorm with Hail","Thunderstorm with light drizzle","Thunderstorm with heavy drizzle", "Heavy Rain", "Moderate Rain", "Light Rain", "Freezing rain","Light shower rain","Shower rain","Heavy shower rain","Overcast Clouds", "Broken Clouds", "Scattered Clouds","Few Clouds","Light Drizzle", "Drizzle", "Heavy Drizzle", ]
    }
}

extension NotificationViewController: UIPickerViewDelegate{
}

extension NotificationViewController: UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerViewData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerViewData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //quanto l'utente preme su un determinato tipo, qua si scrivere quello che deve fare
        weatherType = pickerViewData[row]
    }
    
    //Segue per portarsi dietro il contenuto clieccato dall'untete 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "chooseLocationSegue"{
            let viewController = segue.destination as! ChooseCityNotificationViewController
            viewController.delegate = self
        }
    }
}

extension NotificationViewController: ChooseCityNotificationViewControllerDelegate{
    func didAdd(newLocation: WeatherLocation) {
        //print("Added new location", newLocation.country, newLocation.city)
        chooseCityLabel.text = newLocation.city
        latitude = newLocation.lat
        longitude = newLocation.long
         confirmButton.isHidden = false
        //print("Added new location", newLocation.country, newLocation.city)
        //print("inserita nella label: ", chooseCityLabel)
        
    }
}

