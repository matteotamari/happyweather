//
//  ChooseCityNotificationViewController.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 13/06/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import UIKit

//utilizzo un protoccolo per passare alla table NotificationViewController le varie selezioni dell'utente in merito alle localita' che sceglie
protocol ChooseCityNotificationViewControllerDelegate {
    func didAdd(newLocation: WeatherLocation)
}

class ChooseCityNotificationViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
//    private let notification = NotificationPublisher()
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    var allLocation: [WeatherLocation] = []
    //array che ci serve per scorrere il nostro file e salvare dentro dinamicamente i filitri che l'ultente inserisce quando digita la città
    var filteredLocation: [WeatherLocation] = []
    //arrray per salvare le località preferite dell'utente, setto ad optional perchè la prima volta che accede alla app non avrà location salvate
    var savedLocation: [WeatherLocation]?
    var cityLabel: NotificationViewController?
    var delegate: ChooseCityNotificationViewControllerDelegate?
    //Cons
    let searchController = UISearchController(searchResultsController: nil)
    //let userDefault = UserDefaults.standard
    //view life cycle
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //loadFromUserDefault()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //rimuovo le righe dala table view
        
        
        tableView.tableFooterView = UIView()
        
        setupSearchController()
        
        tableView.tableHeaderView = searchController.searchBar
        // Do any additional setup after loading the view.
        setupTapGesture()
        loadLocationsFromCSV()
        
    }
    
    private func setupSearchController(){
        searchController.searchBar.placeholder = "City or Country"
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        definesPresentationContext = true
        
        searchController.searchBar.searchBarStyle = UISearchBar.Style.prominent
        searchController.searchBar.sizeToFit()
        searchController.searchBar.backgroundImage = UIImage()
    }
    
    //funzione per far ritornare lútente alla home location quando sceglie una localita'
    private func setupTapGesture(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(tableTapped))
        self.tableView.backgroundView = UIView()
        self.tableView.backgroundView?.addGestureRecognizer(tap)
    }
    
    @objc func tableTapped(){
        dismissView()
    }
    
    //getLocation
    private func loadLocationsFromCSV(){
        if let path = Bundle.main.path(forResource: "location", ofType: "csv"){
            parseCVSat(url: URL(fileURLWithPath: path))
        }
    }
    
    private func parseCVSat(url: URL){
        do{
            
            let data = try Data(contentsOf: url)
            let dataEncoded = String(data: data, encoding: .utf8)
            
            if let dataArray = dataEncoded?.components(separatedBy: "\n").map({$0.components(separatedBy: ",")}){
                var i = 0
                // la prima riga del CSV è la descrizione del file, i mi serve per eliminarla, con line.count mi assicuro che venga fatto il parsing delle line che hanno tutti i caratteri che mi servono
                for line in dataArray{
                    if line.count > 6 && i != 0{
                        createLocation(line: line)
                    }
                    i += 1
                }
            }
            
        }catch{
            //se la lettura del csv fornito dalla API owner da un'errore, mi notifica e localizza l'errore per fixare
            print("ERROR during the reading of CSV", error.localizedDescription)
        }
    }
    
    private func createLocation (line: [String]){
        let weatherLocation = WeatherLocation(cityId: "", city: line[1], regionId: "", country: line[3], countryCode: "", lat: line[5], long: line.last, isCurrentLocation: false)
        
        allLocation.append(weatherLocation)
        //stampa di verifica del numero delle line presenti nel CSV
        print(allLocation.count)
    }
    
    private func dismissView(){
        //una volta che l'utente sceglie la localia' chiudo la tableView
        if searchController.isActive{
            searchController.dismiss(animated: true) {
                self.dismiss(animated: true)
            }
        }else{
            self.dismiss(animated: true)
        }
    }
}

extension ChooseCityNotificationViewController: UISearchResultsUpdating{
    //funzione per il filtraggio della lovalità quando l'untente digita
    func filteredContentForSearchText(searchText: String, scope: String = "All"){
        filteredLocation = allLocation.filter({ (weatherLocation) -> Bool in
            return weatherLocation.city.lowercased().contains(searchText.lowercased()) || weatherLocation.country.lowercased().contains(searchText.lowercased())
        })
        
        tableView.reloadData()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredContentForSearchText(searchText: searchController.searchBar.text!)
        
    }
}

extension ChooseCityNotificationViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(filteredLocation.count)
        return filteredLocation.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let location = filteredLocation[indexPath.row]
        cell.textLabel?.text = location.city
        cell.detailTextLabel?.numberOfLines = 0
        cell.detailTextLabel?.text = "\(location.country!), \(location.lat!), \(location.long!)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        //saveToUserDefault(location: filteredLocation[indexPath.row])
        //notifico al delegate   se qualcuno ha inserito una nuova location
        delegate?.didAdd(newLocation: filteredLocation[indexPath.row])
        dismissView()
    }
}




