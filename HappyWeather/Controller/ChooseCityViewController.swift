//
//  ChooseCityViewController.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 31/05/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import UIKit

//utilizzo un protoccolo per passare alla table AllLocation le varie selezioni dell'utente in merito alle localita' che sceglie
protocol ChooseCityViewControllerDelegate {
    func didAdd(newLocation: WeatherLocation)
}

class ChooseCityViewController: UIViewController {
    
    //outlet
    @IBOutlet weak var tableView: UITableView!
    
    //Variable
    var allLocation: [WeatherLocation] = []
    //array che ci serve per scorrere il nostro file e salvare dentro dinamicamente i filitri che l'ultente inserisce quando digita la città
    var filteredLocation: [WeatherLocation] = []
    //arrray per salvare le località preferite dell'utente, setto ad optional perchè la prima volta che accede alla app non avrà location salvate
    var savedLocation: [WeatherLocation]?
    var delegate: ChooseCityViewControllerDelegate?
    //Cons
    let searchController = UISearchController(searchResultsController: nil)
    let userDefault = UserDefaults.standard
    //view life cycle
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadFromUserDefault()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //rimuovo le righe dala table view
        tableView.tableFooterView = UIView()
        
        setupSearchController()
        
        tableView.tableHeaderView = searchController.searchBar
        // Do any additional setup after loading the view.
        setupTapGesture()
        loadLocationsFromCSV()
        
    }
    
    private func setupSearchController(){
        searchController.searchBar.placeholder = "City or Country"
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        definesPresentationContext = true
        
        //serach bar sempre visibile
        searchController.searchBar.searchBarStyle = UISearchBar.Style.prominent
        searchController.searchBar.sizeToFit()
        searchController.searchBar.backgroundImage = UIImage()
    }
    
    //funzione per far ritornare l'utente alle location salvate quando sceglie una localita'(senza selezionare nulla)
    private func setupTapGesture(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(tableTapped))
        self.tableView.backgroundView = UIView()
        self.tableView.backgroundView?.addGestureRecognizer(tap)
    }
    
    //essendo #selector objc style mi serve richiamare la funzione con objc cosi da utilizzare solo quella per la dismiss della tableview
    @objc func tableTapped(){
        dismissView()
    }
    
    //carico la location dal file csv
    private func loadLocationsFromCSV(){
        if let path = Bundle.main.path(forResource: "location", ofType: "csv"){
            parseCVSat(url: URL(fileURLWithPath: path))
        }
    }
    
    //eseguo il parsing del file separando gli elementi con la "," creando per ogni località un array distinto
    private func parseCVSat(url: URL){
        do{
            
            let data = try Data(contentsOf: url)
            let dataEncoded = String(data: data, encoding: .utf8)
            
            if let dataArray = dataEncoded?.components(separatedBy: "\n").map({$0.components(separatedBy: ",")}){
                var i = 0
                // la prima riga del CSV è la descrizione del file, mi serve per eliminarla, con line.count mi assicuro che venga fatto il parsing delle line che hanno tutti i caratteri che mi servono
                for line in dataArray{
                    if line.count > 6 && i != 0{
                        createLocation(line: line)
                    }
                    i += 1
                }
            }
            
        }catch{
            //se la lettura del csv fornito dalla API owner da un'errore, mi notifica e localizza l'errore per fixare
            print("ERROR during the reading of CSV", error.localizedDescription)
        }
    }
    
    private func createLocation (line: [String]){
        let weatherLocation = WeatherLocation(cityId: "", city: line[1], regionId: "", country: line[3], countryCode: "", lat: line[5], long: line.last, isCurrentLocation: false)
        
        //appendo le location varie al mio array di tipo weatherlocation
        allLocation.append(weatherLocation)
        //stampa di verifica del numero delle line presenti nel CSV
        //print(allLocation.count)
    }
    
    //UserDefault
    private func saveToUserDefault(location: WeatherLocation){
        //se è diverso da nil allora abbiamo "n" località salvate dove "n" è un numero compreso tra 1 e infinito
        if savedLocation != nil{
            //controllo che la location non sia già presente nel mio array
            if !savedLocation!.contains(location){
                //aggiungo la località che l'untente inserisce
                savedLocation!.append(location)
            }
        }else {
            //non ci sono località salvate e quindi salvo quela inserita nell'array apposito passandogli come paramentro la location richiesta sall'utente
            savedLocation = [location]
            
        }
        //inserisco la località in UD
        userDefault.set(try? PropertyListEncoder().encode(savedLocation!), forKey: "Locations")
        userDefault.synchronize()
    }
    
    // funzione per verificare se la località esiste già nel DB, cosi da non creare doppie località a dispaly e che occupano UD
    private func loadFromUserDefault(){
        if let data = userDefault.value(forKey: "Locations") as? Data{
            //eseguo il decode per tirare fuori i dati da UD
            savedLocation =  try? PropertyListDecoder().decode(Array<WeatherLocation>.self, from: data)
            //verifico che ci sia qualcosa in user default
            //print(savedLocation?[1].city)
        }
    }
    
    private func dismissView(){
        //una volta che l'utente sceglie la localia' chiudo la tableView
        if searchController.isActive{
            searchController.dismiss(animated: true) {
                self.dismiss(animated: true)
            }
        }else{
            self.dismiss(animated: true)
        }
    }
}

extension ChooseCityViewController: UISearchResultsUpdating{
    //funzione per il filtraggio della località quando l'untente digita
    func filteredContentForSearchText(searchText: String, scope: String = "All"){
        filteredLocation = allLocation.filter({ (weatherLocation) -> Bool in
            //anche se l'utente digita tutto minuscolo o maiuscolo convertiamo per matchare il file csv
            return weatherLocation.city.lowercased().contains(searchText.lowercased()) || weatherLocation.country.lowercased().contains(searchText.lowercased())
        })
        
        tableView.reloadData()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredContentForSearchText(searchText: searchController.searchBar.text!)
        
    }
}

extension ChooseCityViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //ritorno la location che ha raggiunto il risultato di ricerca
        return filteredLocation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let location = filteredLocation[indexPath.row]
        cell.textLabel?.text = location.city
        cell.detailTextLabel?.numberOfLines = 0
        cell.detailTextLabel?.text = "\(location.country!), \(location.lat!), \(location.long!)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        saveToUserDefault(location: filteredLocation[indexPath.row])
        //notifico al delegate se qualcuno ha inserito una nuova location
        delegate?.didAdd(newLocation: filteredLocation[indexPath.row])
        dismissView()
    }
}
