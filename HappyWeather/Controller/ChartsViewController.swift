//
//  ChartsViewController.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 07/06/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import UIKit
import Charts
import TinyConstraints

class ChartsViewController: UIViewController {
    
    @IBOutlet weak var temperatureView: UIView!
    @IBOutlet weak var precipitationView: UIView!
    
    let chartPrepView = BarChartView()
    let chartTempView = LineChartView()
    var chatsForecastData: [ChartsForecast] = []
    var tempData = [ChartDataEntry]()
    var tempWeatherData = LineChartDataSet()
    var prepData = [BarChartDataEntry]()
    var prepWeatherData = BarChartDataSet()
    var chooseDay: Int = 1
    
    
    // var prepare charts to display temperature
    lazy var lineChartTempView: LineChartView = {
        
        //chartView.backgroundColor = .systemYellow
        chartTempView.rightAxis.enabled = false
        
        let yAxis = chartTempView.leftAxis
        yAxis.labelFont = .boldSystemFont(ofSize: 12)
        yAxis.setLabelCount(6, force: false)
        yAxis.labelTextColor = .white
        yAxis.axisLineColor = .white
        yAxis.labelPosition = .outsideChart
        
        chartTempView.xAxis.labelPosition = .bottom
        chartTempView.xAxis.labelFont = .boldSystemFont(ofSize: 12)
        chartTempView.xAxis.setLabelCount(6, force: false)
        chartTempView.xAxis.labelTextColor = .white
        chartTempView.xAxis.axisLineColor = .white
        chartTempView.xAxis.axisLineColor = .systemBlue
        chartTempView.autoresizingMask = temperatureView.autoresizingMask
        
        chartTempView.animate(xAxisDuration: 1)
        return chartTempView
    }()
    
    //var prepare chart to display the precipitation
    lazy var barChartPrepView: BarChartView = {
        
        //chartView.backgroundColor = .systemYellow
        chartPrepView.rightAxis.enabled = false
        chartPrepView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        let yAxis = chartPrepView.leftAxis
        yAxis.labelFont = .boldSystemFont(ofSize: 12)
        yAxis.setLabelCount(6, force: false)
        yAxis.labelTextColor = .white
        yAxis.axisLineColor = .white
        yAxis.labelPosition = .outsideChart
        
        chartPrepView.xAxis.labelPosition = .bottom
        chartPrepView.xAxis.labelFont = .boldSystemFont(ofSize: 12)
        chartPrepView.xAxis.setLabelCount(6, force: false)
        chartPrepView.xAxis.labelTextColor = .white
        chartPrepView.xAxis.axisLineColor = .white
        chartPrepView.xAxis.axisLineColor = .systemBlue
        chartPrepView.autoresizingMask = precipitationView.autoresizingMask
        
        
        chartPrepView.animate(xAxisDuration: 1.5)
        return chartPrepView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        temperatureView.addSubview(lineChartTempView)
        print("ecco il tuo giorno",chooseDay)
        //view.addSubview(lineChartTempView)
        lineChartTempView.centerInSuperview()
        lineChartTempView.width(to: temperatureView)
        lineChartTempView.heightToWidth(of: temperatureView)
        
        precipitationView.addSubview(barChartPrepView)
        barChartPrepView.centerInSuperview()
        barChartPrepView.width(to: precipitationView)
        barChartPrepView.heightToWidth(of: precipitationView)
        
        ChartsForecast.downloadChartForecast(){ (getDataFromChartsForecast) in
            self.chatsForecastData = getDataFromChartsForecast
            print("number of element in our array temperature", self.chatsForecastData.count)
            self.getForecastTempFromCharts()
            self.setChartsTempData()
            self.getForecastPrepFromCharts()
            self.setChartPrepData()
        }
        
    }
    
    private func setChartsTempData(){
        tempWeatherData = LineChartDataSet(entries: tempData, label: "Temperature data over \(chooseDay) days")
        tempWeatherData.drawCirclesEnabled = false
        tempWeatherData.mode = .cubicBezier
        tempWeatherData.lineWidth = 3
        tempWeatherData.setColor(.white)
        tempWeatherData.fill = Fill(color: .white)
        tempWeatherData.fillAlpha = 0.7
        tempWeatherData.drawFilledEnabled = true
        tempWeatherData.drawHorizontalHighlightIndicatorEnabled = false
        tempWeatherData.highlightColor = .systemRed
        
        let tempData = LineChartData(dataSet: tempWeatherData)
        tempData.setDrawValues(false)
        lineChartTempView.data = tempData
    }
    
    private func setChartPrepData(){
        prepWeatherData = BarChartDataSet(entries: prepData, label: "Precipitation data over \(chooseDay) days")
        prepWeatherData.drawValuesEnabled = true
        prepWeatherData.colors = ChartColorTemplates.material()
        //prepWeatherData.drawCirclesEnabled = false
        //prepWeatherData.mode = .cubicBezier
        //prepWeatherData.lineWidth = 3
        //prepWeatherData.setColor(.white)
        //prepWeatherData.fill = Fill(color: .white)
        //prepWeatherData.fillAlpha = 0.7
        //prepWeatherData.drawFilledEnabled = true
        //prepWeatherData.drawHorizontalHighlightIndicatorEnabled = false
        prepWeatherData.highlightColor = .systemRed
        
        let prepData = BarChartData(dataSet: prepWeatherData)
        //prepData.setDrawValues(false)
        barChartPrepView.data = prepData
        //lineChartPrepView.data = prepData
    }
    
    private func getForecastTempFromCharts(){
        if(chatsForecastData[0].temp <= 15.00){
            chartTempView.backgroundColor = .systemBlue
        }else{
            chartTempView.backgroundColor = .systemYellow
        }
        for item in 0...chooseDay{
            let yValue = chatsForecastData[item].temp
            print("ecco la temperatura: ", yValue)
            let xValue = Double(item)
            let chartEntry = ChartDataEntry(x: xValue, y: yValue)
            self.tempData.append(chartEntry)
        }
        
    }
    
    private func getForecastPrepFromCharts(){
        print("precipirtazioni array",chatsForecastData.count)
        if(chatsForecastData[0].precip <= 1){
            chartPrepView.backgroundColor = .systemBlue
        }else{
            chartPrepView.backgroundColor = .systemPurple
        }
        for item in 0...chooseDay{
            let yValue = chatsForecastData[item].precip
            print("your precipitation: ", yValue)
            let xValue = Double(item)
            let chartEntry = BarChartDataEntry(x: xValue, y: yValue)
            self.prepData.append(chartEntry)
        }
    }
    
}

extension ChartsViewController: ChartViewDelegate{
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        print(entry)
    }
}

