//
//  WeatherAlertsViewController.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 21/06/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import UIKit

class WeatherAlertsViewController: UIViewController {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var titleOfAlertsLabel: UILabel!
    @IBOutlet weak var descriptionOfAlertsLabel: UILabel!
    @IBOutlet weak var severityLabel: UILabel!
    @IBOutlet weak var noAlertsLabel: UILabel!
    
    var alerts = WeatherAlerts()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        alerts.downloadWeatherAlerts { (success) in
            self.getAlerts()
        }
    }
    
    func getAlerts(){
        if alerts.description != "" && alerts.title != "" && alerts.city != ""{
            cityLabel.text = ("City: \(alerts.city)")
            severityLabel.text = ("Severity: \(alerts.severity)")
            titleOfAlertsLabel.numberOfLines = 0
            titleOfAlertsLabel.text = ("Title : \(alerts.title)")
            descriptionOfAlertsLabel.numberOfLines = 0
            descriptionOfAlertsLabel.text = ("Description: \(alerts.description)")
            noAlertsLabel.text = ""
        }else{
            cityLabel.text = ""
            severityLabel.text = ""
            titleOfAlertsLabel.text = ""
            descriptionOfAlertsLabel.text = ""
            noAlertsLabel.text = "There are no Alerts in \(alerts.city) right now!"
        }
    }
}
