//
//  APIURL.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 31/05/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import Foundation
//first API 30ecc7da56ed41faa0157e10cc7051fd
//second API 9bb6d7920a19475899420656748caee8
//third API 7a57399c107d4c15ac4516499141c7df
//fourth PI a2add54842cd4b8d93a121b0f9657096
let CURRENT_LOCATION_URL = "https://api.weatherbit.io/v2.0/current?&lat=\(LocationService.shared.latitude!)&lon=\(LocationService.shared.longitude!)&key=a2add54842cd4b8d93a121b0f9657096"
let CURRENT_LOCATION_WEEKLYFORECAST_URL = "https://api.weatherbit.io/v2.0/forecast/daily?&lat=\(LocationService.shared.latitude!)&lon=\(LocationService.shared.longitude!)&days=7&key=a2add54842cd4b8d93a121b0f9657096"
let CURRENT_LOCATION_HOURLYFORECAST_URL = "https://api.weatherbit.io/v2.0/forecast/hourly?lat=\(LocationService.shared.latitude!)&lon=\(LocationService.shared.longitude!)&hours=24&key=a2add54842cd4b8d93a121b0f9657096"
let CURRENT_LOCATION_CHARTS_WEEKLYFORECAST_URL = "https://api.weatherbit.io/v2.0/forecast/daily?&lat=\(LocationService.shared.latitude!)&lon=\(LocationService.shared.longitude!)&days=16&key=a2add54842cd4b8d93a121b0f9657096"
let CURRENT_LOCATION_WEATHER_ALERTS = "https://api.weatherbit.io/v2.0/alerts?lat=\(LocationService.shared.latitude!)&lon=\(LocationService.shared.longitude!)&key=a2add54842cd4b8d93a121b0f9657096"

