//
//  GlobalHelpers.swift
//  HappyWeather
//
//  Created by Matteo Tamari on 25/05/2020.
//  Copyright © 2020 Matteo Tamari. All rights reserved.
//

import Foundation
import UIKit
//funzione creata per gestire il cambiamento della data che viene fornita in UNix dalla API
func currentDateFromUnix (unixDate: Double?) -> Date?{
    if unixDate != nil{
        return Date(timeIntervalSince1970: unixDate!)
    }else{
        return Date()
    }
}

// funzione che converte la stringa della immagine ricevuta in input dal file json in una UIImage
func getWeatherIcon(_ type: String) -> UIImage?{
    return UIImage(named: type)
}

